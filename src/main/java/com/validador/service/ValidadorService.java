package com.validador.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.validador.model.Cliente;
import com.validador.model.Expressao;
import com.validador.repository.IClienteRepository;
import com.validador.repository.IExpressaoRepository;

@Service
public class ValidadorService {
	
	@Autowired
	private IExpressaoRepository expressaoRepository;
	
	@Autowired
	private IClienteRepository clienteRepository;
	
	@Transactional(readOnly = false)
	public void saveExpressao(Expressao expressao, Object endereco) {	
		int a = 1;
		String enderecoCliente = endereco.toString();
		if (!enderecoCliente.equals("null")) {
			Cliente cliente;	
			
			Optional<Cliente> clienteOpt = clienteRepository.findByEndereco(enderecoCliente);
			
			if (clienteOpt.isPresent()) {
				cliente = clienteOpt.get();
			} else {
				cliente = new Cliente(enderecoCliente);
			}
			
			expressao.setCliente(cliente);
		}
	
		expressaoRepository.save(expressao);
	}
	
	public List<Expressao> atribuirListaAplicada(String enderecoCliente) {	
		Optional<Cliente> cliente = clienteRepository.findByEndereco(enderecoCliente);

		if (cliente.isPresent()) {
			return expressaoRepository.findByClienteId(cliente.get().getId());
		}
		
		return new ArrayList<>();
	}
	
	public List<Expressao> atribuirListaGlobal() {	
		return expressaoRepository.findByClienteIdIsNull();
	}
	
	public String checkRegex(String url, List<Expressao> list) {		
		for(Expressao exp : list) {
			if (url.matches(exp.getRegex())) {
				return exp.getRegex();
			}
		}
		
		return null;
	}
	
	public String validarUrl(String url, String enderecoCliente) {
		String regex = null;
		List<Expressao> expressoesGlobais = this.atribuirListaGlobal();
		List<Expressao> expressoesAplicadas = this.atribuirListaAplicada(enderecoCliente);
		
		regex = this.checkRegex(url, expressoesAplicadas);
		
		if (regex == null)
			regex = this.checkRegex(url, expressoesGlobais);
		
		return regex;
	}

}
