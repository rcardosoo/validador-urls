FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/validador-urls-*.jar app.jar
EXPOSE 3200
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-Dmaven.test.skip=true", "-jar", "/app.jar"]