package com.validador.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.validador.model.Expressao;

public interface IExpressaoRepository extends JpaRepository<Expressao, Long> {

	public List<Expressao> findByClienteId(Long id);

	public List<Expressao> findByClienteIdIsNull();

}
