package com.validador.sender;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySource("file:dockercompose.yml")
public class ResponseExchangeSender {
	
	@Autowired
	private Environment env;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	public void send(String message) {
		rabbitTemplate.convertAndSend(
				env.getProperty("RESPONSE_EXCHANGE"),
				env.getProperty("RESPONSE_ROUTING_KEY"),
				message);
	}

}
