package com.validador;

import javax.sql.DataSource;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@SpringBootApplication
@EnableRabbit
@PropertySource("file:dockercompose.yml")
public class ValidadorUrlsApplication {

	@Autowired
	private Environment env;

	public static void main(String[] args) {
		SpringApplication.run(ValidadorUrlsApplication.class, args);
	}

	@Bean
	public DataSource dataSource() {
		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
		dataSourceBuilder.url(env.getProperty("JDBC_URL"));
		return dataSourceBuilder.build();   
	}

}
