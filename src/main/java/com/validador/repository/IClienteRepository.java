package com.validador.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.validador.model.Cliente;

public interface IClienteRepository extends JpaRepository<Cliente, Long> {

	public Optional<Cliente> findByEndereco(String cliente);

}
