package com.validador.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.validador.model.Expressao;
import com.validador.sender.ResponseExchangeSender;
import com.validador.service.ValidadorService;

@Component
public class ValidadorController {
	
	@Autowired
	private ValidadorService validadorService;
	
	@Autowired
	private ResponseExchangeSender responseExchangeSender;
	
	public void save(String mensagem) {
		JSONObject request = new JSONObject(mensagem);
		validadorService.saveExpressao(new Expressao(request.getString("regex")), request.get("client"));
	}
	
	public String validarUrl(String mensagem) {
		JSONObject request 	= new JSONObject(mensagem);
		String client 		= request.getString("client");
		String url 			= request.getString("url");
		int correlationId 	= request.getInt("correlationId");
		
		String matchedRegex = validadorService.validarUrl(url, client);
		String response = this.montarRetorno(matchedRegex, correlationId).toString();
		responseExchangeSender.send(response);
		return response;
	}
	
	public JSONObject montarRetorno(String regex, int correlationId) {
		JSONObject retorno = new JSONObject();

		if (regex != null) {
			retorno.put("match", true);
			retorno.put("regex", regex);
		} else {
			retorno.put("match", false);
			retorno.put("regex", JSONObject.NULL);
		}
		
		retorno.put("correlationId", correlationId);
		return retorno;
	}
}
