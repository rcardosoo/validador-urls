package com.validador.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.validador.controller.ValidadorController;

@Component
@PropertySource("file:dockercompose.yml")
public class MessageConsumer {
	
	@Autowired
	private ValidadorController validadorController;
	
    @RabbitListener(queues = {"${INSERTION_QUEUE}"})
	public void insertionQueueeReceiver(@Payload String message) {
    	validadorController.save(message);
	}
    
    @RabbitListener(queues = {"${VALIDATION_QUEUE}"})
	public void validationQueueeReceiver(@Payload String message) {
    	validadorController.validarUrl(message);
	}
}	
