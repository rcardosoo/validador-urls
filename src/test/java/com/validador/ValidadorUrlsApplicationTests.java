package com.validador;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.validador.controller.ValidadorController;
import com.validador.model.Cliente;
import com.validador.model.Expressao;
import com.validador.repository.IClienteRepository;
import com.validador.repository.IExpressaoRepository;
import com.validador.service.ValidadorService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ValidadorUrlsApplicationTests {
	
	@Autowired
	private ValidadorController validadorController;
	
	@Autowired
	private ValidadorService validadorService;
	
	@Autowired
	private IClienteRepository clienteRepository;
	
	@Autowired
	private IExpressaoRepository expressaoRepository;
	
	private Cliente cliente = new Cliente("123.345.56.7");
	private Expressao expressao = new Expressao("\\S+");
	
	@Before
	public void setup() {
		expressaoRepository.deleteAllInBatch();
		clienteRepository.deleteAllInBatch();
		
		validadorService.saveExpressao(this.expressao, this.cliente.getEndereco());
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void validarUrlAplicada() {
		int correlationId = 1;
		String mensagem = "{\"client\": "+this.cliente.getEndereco()+","
				+ " \"url\": \"http://oi.com.br\","
				+ " \"correlationId\": "+correlationId+"}}";
		
		String resultStr = validadorController.validarUrl(mensagem);
		JSONObject result = new JSONObject(resultStr);
		
        Assert.assertNotEquals(result.getString("regex"), JSONObject.NULL);
        Assert.assertEquals(result.getString("regex"), this.expressao.getRegex());
        Assert.assertEquals(result.getBoolean("match"), true);
        Assert.assertEquals(result.getInt("correlationId"), correlationId);
	}
	
}
